// Ejercicio 1: Imprimir mensaje en consola
/* Descripción: Este código imprime el mensaje "Hello world xd" 
en la consola del navegador.*/
console.log("Hello world xd");

// Ejercicio 2: Declaración de variables con diferentes alcances
/* Descripción: Aquí se declaran tres variables, 's', 'x' y 'y', 
con diferentes alcances.*/
/* 's' es una variable global, 'x' tiene alcance de bloque, y 'y'
 es una variable global.*/
var s = "Foo Bar";  
let x = 90;        
var y = 89;     

// Ejercicio 3: Declaración de un array con diversos tipos de datos
/* Descripción: Se declara un array llamado 'array' que contiene números, 
cadenas, booleanos y decimales.*/
var array = [1, 2, 3, 4, 5, "Foo", "Bar", true, false, 2.34, 4.23];

// Ejercicio 4: Declaración de un objeto con propiedades y un array anidado
/* Descripción: Se crea un objeto 'obj' con varias propiedades, incluyendo un 
array llamado 'arr' que se inicializa con el array anterior.*/
var obj = { 
   first_name: "Foo",
   last_name: "Bar", 
   age: 23, 
   city: "TJ", 
   status: true , 
   arr: array 
};

// Ejercicio 5: Bucle for que imprime números del 0 al 99
// Descripción: Este bucle for imprime los números del 0 al 99 en la consola.
for (let i = 0; i < 100; i++) {
   console.log(i);
}

// Ejercicio 6: Bucle for que imprime cada elemento del array
/* Descripción: Se utiliza un bucle for para recorrer el array 'array' 
e imprimir cada uno de sus elementos.*/
for (let i = 0; i < array.length; i++) {
   console.log(array[i]);
}

// Ejercicio 7: Bucle for...of incorrecto, debe ser for...of array, no array.length
/* Descripción: Este bucle for...of tiene un error, ya que debería iterar 
sobre el array 'array', no sobre su longitud.*/
for (let i of array.length) {
   console.log(array[i]);
}

// Ejercicio 8: Bucle for...of que itera a través de las claves del objeto
/* Descripción: Se utiliza un bucle for...of para recorrer las claves del 
objeto 'obj' e imprimir sus valores correspondientes.*/
for(let key of Object.keys(obj)) {
   console.log(key + ": " + obj[key]);
}

// Ejercicio 9: Bucle for...in que itera a través de las propiedades del objeto
/* Descripción: Este bucle for...in recorre las propiedades del objeto 'obj' 
e imprime sus valores.*/
for(let key in obj) {
   console.log(key + ": " + obj[key]);
}

// Ejercicio 10: Bucle while que multiplica i por 5 hasta que sea mayor o igual a 1000
/* Descripción: Se utiliza un bucle while que multiplica la variable 'i' 
por 5 hasta que alcanza o supera el valor de 1000.*/
var i = 1;
while(i < 1000){
   i *= 5;
   console.log(i);
}

// Ejercicio 11: Bucle do...while que aumenta o disminuye i hasta llegar a 1000 o 1
/* Descripción: Un bucle do...while que incrementa o decrementa la variable 
'i' hasta alcanzar los valores 1000 o 1, alternativamente.*/
var l = true;
do {
   console.log(i);
   if (i == 1000) {
       l = false;
   } else if (i == 1) {
       l = true;
   }
   if (l) {
       i *= 10;
   } else {
       i /= 10;
   }
} while(true);

// Ejercicio 12: Función que espera una cantidad específica de milisegundos
/* Descripción: Se define

 una función 'esperar' que devuelve una Promesa 
y espera la cantidad de milisegundos especificada.*/
function esperar(milliseconds) {
 return new Promise(resolve => setTimeout(resolve, milliseconds));
}

// Ejercicio 13: Función asíncrona que espera 2 segundos antes de imprimir un mensaje
/* Descripción: Una función asíncrona 'miFuncionConEspera' que imprime 
un mensaje antes y después de esperar 2 segundos utilizando la función 'esperar'.*/
async function miFuncionConEspera() {
 console.log("Inicio de la función");
 await esperar(2000);
 console.log("Fin de la función después de esperar 2 segundos");
}

// Ejercicio 14: Llamada a la función asíncrona
/* Descripción: Se realiza la llamada a la función asíncrona 'miFuncionConEspera', 
la cual imprimirá mensajes antes y después de esperar.*/
miFuncionConEspera();

// Ejercicio 15: Reasignación de la variable s a true
/* Descripción: Se reasigna el valor de la variable 's' a 'true'.*/
s = true;

// Ejercicio 16: Impresión de distintas variables y expresiones
/* Descripción: Se imprimen en consola diferentes variables y expresiones, 
mostrando diversos tipos de datos y operaciones.*/
console.log(s);
console.log(x + y);
console.log(s);
console.log(array);
console.log(array[5]);
console.log(obj);
console.log(obj["first_name"]);
console.log(obj.arr);

// Ejercicio 17: Estructura condicional if...else
/* Descripción: Se utiliza una estructura if...else para comparar las variables 
'x' e 'y' e imprimir un mensaje correspondiente.*/
if (x > y) {
   console.log("si");
} else {
   console.log("no");
}

// Ejercicio 18: Estructura condicional switch
/* Descripción: Se implementa una estructura switch para evaluar el valor 
de la variable 'opc' e imprimir mensajes según los casos.*/
var opc = 1;
switch (opc) {
   case 1:
       console.log("1");
       break;
   case 2:
       console.log("2");
       break;
   case 3:
       console.log("3");
       break;
   default:
       console.log("default");
       break;
}

// Ejercicio 19: Operador ternario
/* Descripción: Se utiliza el operador ternario para determinar si la 
variable 'animal' es igual a "Kitty" y asignar el mensaje correspondiente.*/
var animal = "Kitty";
var hello = (animal === "Kitty") ? "It is a pretty kitty" : "It is not a pretty kitty";
console.log(hello);

// Ejercicio 20: Función anidada
/* Descripción: Se define una función 'foo' que contiene una función 
anidada 'xd', la cual imprime el valor de la variable 'a'.*/
function foo() {
   var a = "kitty";
   function xd() {
       console.log(a);
   }
   xd();
}

// Ejercicio 21: Función de cálculo del volumen de un prisma
/* Descripción: Se declara una función 'prism' que calcula el volumen de 
un prisma rectangular dado sus dimensiones.*/
var prism = function(l, w, h) {
   return l * w * h;
};
console.log("Volumen del prisma:" + prism(23, 56, 12));

// Ejercicio 22: Función que retorna otra función para calcular el volumen de un prisma
/* Descripción: Se define una función 'prisma' que devuelve una función 
anidada para calcular el volumen del prisma.*/
function prisma(l) {
   return function(w) {
       return function(h) {
           return l * w * h;
       };
   };
}
console.log("Volumen del prisma: " + prisma(23)(12)(56));

// Ejercicio 23: Función auto-invocada
/* Descripción: Se utiliza una función auto-invocada (IIFE) para imprimir 
un mensaje sobre las papas.*/
const foo = (function() {
    console.log("Me gustan los potatos");
 }());
 
 // Ejercicio 24: Declaración de una función con nombre y una función anónima
 /* Descripción: Se declaran dos funciones, una con nombre 'namedsum' y otra 
 anónima 'anonSum', ambas realizan la suma de dos valores.*/
 var namedsum = function sum(a, b) {
    return a + b;
 };
 
 var anonSum = function(a, b) {
    return a + b;
 };
 console.log(anonSum(5, 5));
 
 // Ejercicio 25: Función recursiva
 /* Descripción: Se define una función recursiva 'say' que imprime "hello" 
 un número determinado de veces.*/
 var say = function say(times) {
    say = undefined;
    if (times > 0) {
        console.log("hello");
        say(times - 1);
    }
 };
 
 var saysay = say;
 say = "ops";
 saysay(5);
 console.log(say);
 
 // Ejercicio 26: Función con un parámetro predeterminado
 /* Descripción: Se define una función 'foo'con un parámetro predeterminado 
 que imprime un mensaje.*/
 function foo(msg = "Me gustan las naranjas") {
    console.log(msg);
 }
 foo();
 
 // Ejercicio 27: Llamada a una API usando fetch para obtener datos de Stack Exchange y JSONPlaceholder
 /* Descripción: Se realizan llamadas a dos APIs diferentes (Stack Exchange 
 y JSONPlaceholder) utilizando fetch para obtener y mostrar datos.*/
 
 var urlStackExchange = "https://api.stackexchange.com/2.2/questions/featured?order=desc&sort=activity&site=stackoverflow";
 var responseDataStackExchange = fetch(urlStackExchange).then(response => response.json());
 responseDataStackExchange.then(({items, has_more, quota_max, quota_remaining}) => {
    for (var {title, score, owner, link, anwser_count} of items) {
        console.log('Q:' + title + " owner: " + owner.display_name);
    }
 });
 
 var urlJsonPlaceholderUsers = "https://jsonplaceholder.typicode.com/users";
 var responseDataUsers = fetch(urlJsonPlaceholderUsers).then(response => response.json())
                             .then(response => {
                                response.forEach(user => {
                                    console.log(user.username);
                                });
                              });
 
 var urlJsonPlaceholderPosts = "https://jsonplaceholder.typicode.com/posts";
 fetch(urlJsonPlaceholderPosts).then(response => response.json())
         .then(response => {
            response.forEach(posts => {
                console.log(posts.title);
            });
          });
