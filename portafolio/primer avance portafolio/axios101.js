// Librería Axios para hacer solicitudes HTTP
const axios = require("axios")

// URL de la API JSONPlaceholder para usuarios
const url = "https://jsonplaceholder.typicode.com/users"

// Ejercicio 28: Hacer una solicitud GET para obtener datos de usuarios
// Descripción: Se utiliza Axios para hacer una solicitud GET a la API JSONPlaceholder y se imprime el nombre de usuario de cada usuario en la respuesta.

axios.get(url).then(response => {
     response.data.forEach(element => {
       console.log(element.username)
    });
 })

// Ejercicio 29: Hacer una solicitud POST para agregar un nuevo usuario
// Descripción: Se utiliza Axios para hacer una solicitud POST a la API JSONPlaceholder, enviando un nuevo usuario con nombre de usuario y correo electrónico. 
// La respuesta del servidor se imprime en la consola.

axios.post(url, {
    username: "Foo Bar",
    email: "Foo bar"
}).then(response => console.log(response.data))
